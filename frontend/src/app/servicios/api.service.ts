import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class APIService {

  constructor(private _httpClient: HttpClient) { }

  getAll(){
    return this._httpClient.get('http://localhost:3000/users');
  }

  getOne(id: number){
    return this._httpClient.get(`http://localhost:3000/users/${id}`);
  }

  create(body: User){
    return this._httpClient.post('http://localhost:3000/users', body)
  }

  editUser(body: User){
    return this._httpClient.put(`http://localhost:3000/users/${body.id}`, body);
  }

  deleteOne(id: number){
    return this._httpClient.delete(`http://localhost:3000/users/${id}`);
  }
}
