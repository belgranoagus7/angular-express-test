import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private _formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
  }

  login: FormGroup = this._formBuilder.group({
    username: ['', []],
    password: ['', []],
  })

}
