import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListaRoutingModule } from './lista-routing.module';
import { ListComponent } from './list/list.component';
import { UtilsModule } from '../utils/utils.module';
import { EditComponent } from './edit/edit.component';
import { QuestionComponent } from './question/question.component';


@NgModule({
  declarations: [
    ListComponent,
    EditComponent,
    QuestionComponent,
  ],
  imports: [
    CommonModule,
    ListaRoutingModule,
    UtilsModule
  ]
})
export class ListaModule { }
