import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/models/user.model';
import { APIService } from 'src/app/servicios/api.service';
import { EditComponent } from '../edit/edit.component';
import { QuestionComponent } from '../question/question.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  userList!: User[];
  displayedColumns = ['name', 'surname', 'email', 'username', 'config'];

  constructor(
    private _apiService: APIService,
    private _dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this._apiService.getAll().subscribe({
      next: (x: any) => {
        this.userList = (x);
      }
    })
  }

  editUser(id: number) {
    let user = this.userList.find(user => user.id == id);

    const dialogRef = this._dialog.open(EditComponent, {
      width: '450px',
      data: {
        id: user?.id,
        name: user?.name,
        surname: user?.surname,
        email: user?.email,
        username: user?.username,
        password: user?.password,
      }
    })
    dialogRef.afterClosed().subscribe();
  }

  deleteUser(id: number){
    let user = this.userList.find(user => user.id == id);

    const dialogRef = this._dialog.open(QuestionComponent, {
      width: '250px',
      data: {
        id: user?.id,
        name: user?.name,
        surname: user?.surname,
        email: user?.email,
        username: user?.username,
        password: user?.password,
      }
    })
    dialogRef.afterClosed().subscribe();
  }

}
