import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/user.model';
import { APIService } from 'src/app/servicios/api.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  
  isActive!: boolean;

  constructor(
    private _apiService: APIService,
    private _formBuilder: FormBuilder,
    private _dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,   
    ) { }

  ngOnInit(): void {
  }

  form: FormGroup = this._formBuilder.group({
    id: [this.data.id, []],
    name: [ this.data.name ,[]],
    surname: [this.data.surname ,[]],
    email: [this.data.email,[]],
    username: [this.data.username,[]],
    password: [this.data.password,[]],
  })

  closeDialog(){
    this._dialogRef.close();
  }

  
  saveEdit(body: User){    
    this._apiService.editUser(body).subscribe();    
  }

}
