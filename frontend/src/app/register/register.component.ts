import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { APIService } from '../servicios/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  userList!: User[];

  constructor(    
    private _formBuilder: FormBuilder,
    private _APIService: APIService
  ) { }

  ngOnInit(): void {
    this._APIService.getAll().subscribe({
      next: (x: any) => {
        this.userList = x;
      }
    })
  }

  form: FormGroup = this._formBuilder.group({
    name: ['',[Validators.required]],
    surname: ['',[Validators.required]],
    email: ['',[Validators.required]],
    username: ['',[Validators.required]],
    password: ['',[Validators.required]],
  })

  create(form: any){
    this._APIService.create(form.value).subscribe()
  }


}
