const express = require('express');
const app = express();
const port = 3000;
const routes = require('./routes/user.route');
const cors = require('cors');

app.use(express.json());

app.use(cors());

// ruteo de controllers
app.use('/users', routes);

// error handler
app.use((req,res) => {
    res.sendStatus(404);
})

app.use((err, req, res, next) => {
    res.status(500).send(err.message)
})

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})