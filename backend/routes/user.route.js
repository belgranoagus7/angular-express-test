const router = require('express').Router();
const userController = require('../controllers/user.controller');


router.get('/', userController.getAll)
router.get('/:id', userController.getOne)
router.post('/', userController.create)
router.put('/:id', userController.editUser)
router.delete('/:id', userController.deleteUser);

module.exports = router